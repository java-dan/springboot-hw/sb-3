import React, { useState } from 'react';

import Button from '@mui/material/Button';

export default function AccountForm({ onSave, onCancel }) {
  const [currency, setCurrency] = useState('');

  const handleSaveAccount = () => {
    onSave({ currency });
    setCurrency('');
  };

  return (
     <div>
         <h2>Add Account</h2>
         <form>
           <label>
             Currency:
             <select value={currency} onChange={(e) => setCurrency(e.target.value)}>
               <option value="">Select Currency</option>
               <option value="USD">USD</option>
               <option value="EUR">EUR</option>
               <option value="UAH">UAH</option>
               <option value="CHF">CHF</option>
               <option value="GBP">GBP</option>
             </select>
           </label>
           <br />
           <Button variant="outlined" size="small" onClick={()=>handleSaveAccount()}>
             Save Account
           </Button>
           <Button variant="outlined" size="small" onClick={onCancel}>
             Cancel
           </Button>
         </form>
       </div>
  );
}
