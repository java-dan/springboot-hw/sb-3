import React, { useState } from 'react';

import Button from '@mui/material/Button';

export default function EmployerForm({ onSave, onCancel }) {
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');

  const handleSaveEmployer = () => {
    onSave({ name, address });
    setName('');
    setAddress('');

    onCancel();
  };

  return (
     <div>
         <h2>Add Employer</h2>
         <form>
           <label>
             Name:
             <input
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
            />
           </label>
           <label>
            Address:
            <input
                type="text"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
            />
           </label>
           <br />
           <Button variant="outlined" size="small" onClick={handleSaveEmployer}>
             Save Employer
           </Button>
           <Button variant="outlined" size="small" onClick={onCancel}>
             Cancel
           </Button>
         </form>
       </div>
  );
}
