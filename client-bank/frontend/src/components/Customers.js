import React, { useEffect, useState } from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';

import Title from './Title';
import AccountForm from './AccountForm';
import CustomerForm from './CustomerForm';
import EmployerForm from './EmployerForm';

export default function Customers() {
  const [customers, setCustomers] = useState([]);
  const [editingCustomerId, setEditingCustomerId] = useState(null);
  const [addACustomerId, setAddACustomerId] = useState(null);
  const [addECustomerId, setAddECustomerId] = useState(null);

  useEffect(() => {
    fetchCustomers();
  }, []);

  const fetchCustomers = async () => {
    try {
      const response = await fetch('http://localhost:9000/customers');
      const data = await response.json();
      setCustomers(data);
    } catch (error) {
      console.error('Error fetching customers:', error);
    }
  };


  const updateCustomer = async (customerId, updatedCustomer) => {
    try {
      const response = await fetch(`http://localhost:9000/customers/${customerId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(updatedCustomer),
      });

      if (response.ok) {
        const customer = await response.json();

        setCustomers((prev) => prev.map((c) => (
          c.id !== customerId ? c : customer)))

        alert('Updating successful');
      } else {
        alert('Failed. Please try again.');
      }
      setEditingCustomerId(null);
    }
    catch (error) {
      console.error('Error updating customer:', error);
    }
  };

  const deleteCustomer = async (customerId) => {
    try {
      const response = await fetch(`http://localhost:9000/customers/${customerId}`, {
        method: 'DELETE',
      });

      if (response.ok) {
        alert('Delete successful');
      } else {
        alert('Failed. Please try again.');
      }
    } catch (error) {
      console.error('Error deleting customer:', error);
    }
  };

  const addAccountToCustomer = async (customerId, account) => {
    try {
      const response = await fetch(`http://localhost:9000/accounts/customers/${customerId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(account),
      });

      if (response.ok) {
        alert('Account added');
      } else {
        alert('Failed. Please try again.');
      }
    } catch (error) {
      console.error('Error adding account to customer:', error);
    }
  }

  const addEmployerToCustomer = async (customerId, employer) => {
    try {
      const response = await fetch(`http://localhost:9000/employers/create/${customerId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(employer),
      });

      if (response.ok) {
        alert('Employer added');
      } else {
        alert('Failed. Please try again.');
      }
    } catch (error) {
      console.error('Error adding employer to customer:', error);
    }
  }

  const deleteEmployerFromCustomer = async (customerId, employerId) => {
    try {
      const response = await fetch(`http://localhost:9000/employers/${employerId}/customers/${customerId}`, {
        method: 'DELETE',
        body: JSON.stringify(employerId),
      })
      if (response.ok) {
        alert('Delete successful');
      } else {
        alert('Failed. Please try again.');
      }
    } catch (error) {
      console.error('Error deleting account from customer:', error);
    }
  }

  const deleteAccount = async (accountId) => {
    try {
      const response = await fetch(`http://localhost:9000/accounts/${accountId}`, {
        method: 'DELETE',
      })
      if (response.ok) {
        alert('Delete successful');
      } else {
        alert('Failed. Please try again.');
      }
    } catch (error) {
      console.error('Error deleting account from customer:', error);
    }
  }

  const depositToAccount = async (customerId, aNumber, amount) => {
    try {
      const response = await fetch(`http://localhost:9000/accounts/deposit/${aNumber}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(amount),
      });

      if (response.ok) {
        const { balance } = await response.json();

        setCustomers((prev) => prev.map((c) => (
          c.id !== customerId ? c : {
            ...c,
            accounts: c.accounts.map((a) => (
              a.number !== aNumber ? a : {
                ...a,
                balance
              }))
          })))

        alert('Deposit successful');
      } else {
        alert('Deposit failed. Please try again.');
      }

    } catch (error) {
      console.error('Error depositing to account:', error);
    }
  };

  const withdrawFromAccount = async (customerId, aNumber, amount) => {
    try {
      const response = await fetch(`http://localhost:9000/accounts/withdraw/${aNumber}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(amount),
      });

      if (response.ok) {
        alert('Withdraw successful');
      } else {
        alert('Not enough money. Please try again.');
      }
    } catch (error) {
      console.error('Error withdrawing from account:', error);
    }
  };

  const handleEditButtonClick = (customerId) => {
    setEditingCustomerId(customerId);
  };

  const handleCancelEdit = () => {
    setEditingCustomerId(null);
  };

  const handleSaveCustomer = (customer) => {
    if (editingCustomerId) {
      updateCustomer(editingCustomerId, customer);
    }
  };

  const handleSaveNewAccount = (customerId, account) => {
      addAccountToCustomer(customerId, account)
  };


  const handleSaveNewEmployer = (customerId, employer) => {
    addEmployerToCustomer(customerId, employer)
  };

  const handleDeleteAcc = (accountId) => {
    deleteAccount(accountId);
  }

  const handleDeleteEmployer = (customerId, employerId) => {
    deleteEmployerFromCustomer(customerId, employerId);
  }

  const handleDeposit = async (customerId, accountId) => {
    const amount = prompt('Enter amount to deposit');
    if (amount <= 0) {
      alert("Amount can't be less than 0");
      return;
    }
    if (amount !== null && amount !== "") {
      depositToAccount(customerId, accountId, amount);
    }
  };

  const handleWithdraw = (customerId, accountId, amount) => {
    if (amount <= 0) {
      alert("Amount can't be less than 0");
      return;
    }
    withdrawFromAccount(customerId, accountId, amount);
  };

  return (
    <React.Fragment>
      {customers.map((customer) => (
        <React.Fragment key={customer.id}>
          {editingCustomerId === customer.id ? (
            <React.Fragment>
              <Title>Editing {customer.name}</Title>
              <CustomerForm
                onSave={handleSaveCustomer}
                onCancel={handleCancelEdit}
                customer={customer}
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <Title>
                {customer.name} (email: {customer.email}, age: {customer.age})
              </Title>
              <h3>
                Accounts
              </h3>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell>Account Number</TableCell>
                    <TableCell>Currency</TableCell>
                    <TableCell>Balance</TableCell>
                    <TableCell>Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {customer.accounts.map((account) => (
                    <TableRow key={account.id}>
                      <TableCell>{account.number}</TableCell>
                      <TableCell>{account.currency}</TableCell>
                      <TableCell>{account.balance}</TableCell>
                      <TableCell>
                        <Button variant="outlined" size="small" onClick={() => handleDeposit(customer.id, account.number)}>
                          Deposit
                        </Button>
                        <Button variant="outlined" size="small" onClick={() => handleWithdraw(customer.id, account.number, prompt('Enter amount to withdraw'))}>
                          Withdraw
                        </Button>
                        <Button variant="outlined" size="small" startIcon={<DeleteIcon />} onClick={() => handleDeleteAcc(account.id)}>
                          Delete
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              <h3>
                Employers
              </h3>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>
                      Employer name
                    </TableCell>
                    <TableCell>
                      Address
                    </TableCell>
                    <TableCell>
                      Actions
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {customer.employers.map((employer) => (
                    <TableRow key={employer.id}>
                      <TableCell>{employer.name}</TableCell>
                      <TableCell>{employer.address}</TableCell>
                      <TableCell>
                        <Button variant="outlined" size="small" startIcon={<DeleteIcon />} onClick={() => handleDeleteEmployer(customer.id, employer.id)}>
                          Delete
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>

              <React.Fragment>
                {addACustomerId === customer.id ? (
                  <AccountForm
                    onSave={(account) => handleSaveNewAccount(customer.id, account)}
                    onCancel={() => setAddACustomerId(null)}
                    customer={customer}
                    id={customer.id}
                  />
                ) : <Link
                  color="primary"
                  href="#"
                  onClick={() => setAddACustomerId(customer.id)}
                  sx={{ mt: 3 }}
                >
                  Add Account
                </Link>}
              </React.Fragment>
              <Link
                color="primary"
                href="#"
                onClick={() => handleEditButtonClick(customer.id)}
                sx={{ mt: 3 }}
              >
                Edit Customer
              </Link>
              <>
                {addECustomerId === customer.id ? (
                  <EmployerForm
                    onSave={(employer) => handleSaveNewEmployer(customer.id, employer)}
                    onCancel={() => setAddECustomerId(null)}
                  />
                ) : (
                  <Link
                    color="primary"
                    href="#"
                    onClick={() => setAddECustomerId(customer.id)}
                    sx={{ mt: 3 }}
                  >
                    Add Employer
                  </Link>
                )}
              </>
              <Button variant="contained" onClick={() => deleteCustomer(customer.id)}>Delete Customer</Button>
            </React.Fragment>

          )}
        </React.Fragment>
      ))}
    </React.Fragment>
  );
};