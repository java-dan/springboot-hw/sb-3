import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';

export default function CustomerForm({ onSave, onCancel, customer }) {
  const [name, setName] = useState(customer ? customer.name : '');
  const [email, setEmail] = useState(customer ? customer.email : '');
  const [age, setAge] = useState(customer ? customer.age : '');
  const [phoneNumber, setPhoneNumber] = useState(customer ? customer.phoneNumber : '');
  const [password, setPassword] = useState(customer ? customer.password : '');

  const createCustomer = async (newCustomer) => {
    try {
      const response = await fetch('http://localhost:9000/customers', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newCustomer),
      })
      if (response.ok) {
        alert("Customer created");

      } else {
        alert('Something get wrong. Not created');
      }
    } catch (error) {
      console.error('Something get wrong', error);
    }
  };

  const handleSave = () => {
    if (age < 18) {
      alert("Enter correct age. More than 18");
      return;
    }
    const updatedCustomer = {
      name,
      email,
      age,
      phoneNumber,
      password

    };
    if (customer === null) {
      createCustomer(updatedCustomer)
      setName("");
      setEmail("");
      setAge("");
      setPhoneNumber("");
      setPassword("");
    } else {
      onSave(updatedCustomer);
    }
  };

  return (
    customer ? (<div>
      <h2>Edit Customer</h2>
      <form>
        <label>
          Name:
          <input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </label>
        <br />
        <label>
          Email:
          <input
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </label>
        <br />
        <label>
          Age:
          <input
            type="number"
            value={age}
            min="18"
            onChange={(e) => setAge(e.target.value)}
          />
        </label>
        <label>
          Phone number:
          <input
            type="text"
            value={phoneNumber}
            onChange={(e) => setPhoneNumber(e.target.value)}
          />
        </label>
        <label>
          Password:
          <input
            type="text"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </label>
        <br />
        <Button variant="outlined" size="small" type="button" onClick={handleSave}>
          Save
        </Button>
        <Button variant="outlined" size="small" onClick={onCancel}>
          Cancel
        </Button>
      </form>
    </div>) : (
      <Box sx={{ mt: 4 }}>
        <TextField
          label="Name"
          variant="outlined"
          value={name}
          onChange={(e) => setName(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Email"
          variant="outlined"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Age"
          variant="outlined"
          type="number"
          value={age}
          min="18"
          onChange={(e) => setAge(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Phone Number"
          variant="outlined"
          value={phoneNumber}
          onChange={(e) => setPhoneNumber(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Password"
          variant="outlined"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          fullWidth
          margin="normal"
        />
        <Button variant="contained" onClick={handleSave}>
          New customer
        </Button>
      </Box>
    )
  );
}
