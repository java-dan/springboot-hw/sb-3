package app;

import app.configuration.YAMLConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Main implements CommandLineRunner {
    @Autowired
    private YAMLConfig yamlConfig;
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Main.class);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.printf("using environment: %s\n", yamlConfig.getEnvironment());
        System.out.printf("using name: %s\n", yamlConfig.getName());
    }
}