package app.controller;

import app.dto.employer.EmployerReq;
import app.dto.employer.EmployerResp;
import app.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employers")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class EmployerController {
    private final EmployerService employerService;

    @GetMapping
    public ResponseEntity<List<EmployerResp>> getAllEmployers() {
        try {
            return ResponseEntity.ok(employerService.getAll());

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployerResp> getEmployer(@PathVariable("id") Long id) {
        return employerService.findById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/create/{customerId}")
    public ResponseEntity<EmployerResp> createEmployer(@PathVariable("customerId") Long cId, @RequestBody EmployerReq e) {
        EmployerResp result = employerService.save(cId, e);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEmployer (@PathVariable Long id){
        return employerService.deleteById(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{employerId}/customers/{customerId}")
    public ResponseEntity<?> deleteEmployerFromCustomer (@PathVariable(value = "employerId") Long employerId, @PathVariable(value = "customerId") Long customerId){
        return employerService.deleteEmployerFromCustomer(customerId, employerId)
            ? ResponseEntity.ok().build()
            : ResponseEntity.notFound().build();
    }
}
