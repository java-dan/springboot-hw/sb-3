package app.controller;

import app.dto.customer.CustomerReq;
import app.dto.customer.CustomerResp;
import app.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Log4j2
@Validated
@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<CustomerResp>> getAllCustomers(@RequestParam(defaultValue = "0") int page,
                                                              @RequestParam(defaultValue = "10") int size
    ) {
        try {
            PageRequest pr = PageRequest.of(page, size);
            return ResponseEntity.ok(customerService.getAll(pr));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerResp> getCustomer(@PathVariable("id") Long id){
        return customerService.findById(id).map(ResponseEntity::ok).orElseThrow();
    }

    @PostMapping
    public ResponseEntity<CustomerResp> createCustomer(@Valid @RequestBody CustomerReq c) {
        return ResponseEntity.ok().body(customerService.save(c));
    }

    @PutMapping("/{id}")
    public ResponseEntity<CustomerResp> updateCustomer(@PathVariable("id") Long id, @Valid @RequestBody CustomerReq c) {
            return customerService.updateCustomer(id, c).map(ResponseEntity::ok).orElseGet(()-> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id) {
        return customerService.deleteById(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }
}
