package app.controller;

import app.dto.account.AccountReq;
import app.dto.account.AccountResp;
import app.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class AccountController {
    private final AccountService accountService;

    @GetMapping
    public List<AccountResp> getAllAccounts() {
        return accountService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<AccountResp> getAccount(@PathVariable("id") Long id) {
        return accountService.findById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/deposit/{number}")
    public ResponseEntity<AccountResp> addFunds(@PathVariable("number") String number, @RequestBody Double amount) {
        try {
            return ResponseEntity.ok(accountService.addFunds(number, amount));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("/withdraw/{number}")
    public ResponseEntity<AccountResp> withdraw(@PathVariable("number") String number, @RequestBody Double amount) {
        try {
            return ResponseEntity.ok(accountService.withdraw(number, amount));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/transfer/{from}/{to}")
    public ResponseEntity<Boolean> transfer(@PathVariable("from") String from, @PathVariable("to") String to, @RequestBody Double amount) {
        boolean b = accountService.transfer(from, to, amount);
        if (b){
            return ResponseEntity.ok(b);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/customers/{customerId}")
    public ResponseEntity<AccountResp> createAccount(@PathVariable("customerId") Long cId, @RequestBody AccountReq a) {
        AccountResp result = accountService.save(cId, a);
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAccount(@PathVariable("id") Long id) {
        return accountService.deleteById(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }
}
