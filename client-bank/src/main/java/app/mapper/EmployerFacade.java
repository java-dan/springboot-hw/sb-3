package app.mapper;

import app.dto.employer.EmployerReq;
import app.dto.employer.EmployerResp;
import app.models.Employer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EmployerFacade {
    private final ModelMapper mapper;

    public Employer toEmployer(EmployerReq er) {
        return mapper.map(er, Employer.class);
    }

    public EmployerResp toEmployerResp(Employer e) {
        EmployerResp resp = mapper.map(e, EmployerResp.class);
        resp.setCreated_date(e.getCreatedDate());
        resp.setLast_modified_date(e.getLastModifiedDate());
        return resp;
    }
}
