package app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name="customers")
@EqualsAndHashCode(callSuper = true)
public class Customer extends AbstractEntity{
    private String name;
    private String email;
    private Integer age;

    private String password;
    private String phoneNumber;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties("customers")
    private List<Account> accounts = new ArrayList<>();

    @ManyToMany
    @JsonIgnoreProperties("customers")
    private List<Employer> employers = new ArrayList<>();

    public void addEmployer(Employer e) {
        this.employers.add(e);
        e.getCustomers().add(this);
    }

    public void removeEmployer(Long eId) {
        Employer employer = this.employers.stream().filter(e -> Objects.equals(e.getId(), eId)).findFirst().orElse(null);
        if(employer != null) {
            this.employers.remove(employer);
            employer.getCustomers().remove(this);
        }
    }
}
