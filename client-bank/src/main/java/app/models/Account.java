package app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="accounts")
@EqualsAndHashCode(callSuper = true)
public class Account extends AbstractEntity{
    private String number = UUID.randomUUID().toString();
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private Double balance = (double) 0;

    @ManyToOne
    @JsonIgnoreProperties("accounts")
    private Customer customer;
}
