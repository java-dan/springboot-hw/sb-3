package app.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="employers")
@EqualsAndHashCode(callSuper = true)
public class Employer extends AbstractEntity{
    private String name;
    private String address;

    @ManyToMany(mappedBy = "employers")
    @JsonIgnoreProperties("employers")
    private List<Customer> customers = new ArrayList<>();
}
