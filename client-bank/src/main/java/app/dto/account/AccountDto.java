package app.dto.account;

import app.models.Currency;
import lombok.Data;

@Data
public class AccountDto {
    private Long id;
    private String number;
    private Currency currency;
    private Double balance;

}
