package app.dto.account;

import app.models.Currency;
import app.models.Customer;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class AccountResp {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String number = UUID.randomUUID().toString();;
    private Currency currency;
    private Double balance = (double) 0;
    private Customer customer;
}
