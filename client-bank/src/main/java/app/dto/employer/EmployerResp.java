package app.dto.employer;

import app.models.Customer;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class EmployerResp {
    private Long id;
    private LocalDateTime created_date;
    private LocalDateTime last_modified_date;
    private String name;
    private String address;
    private List<Customer> customers;
}
