package app.service;

import app.dto.account.AccountReq;
import app.dto.account.AccountResp;
import app.mapper.AccountFacade;
import app.models.Account;
import app.repo.AccountRepo;
import app.repo.CustomerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepo accountRepo;
    private final CustomerRepo customerRepo;
    private final AccountFacade facade;
    public List<AccountResp> getAll() {
        return accountRepo.findAll()
                .stream()
                .map(facade::toAccountResp)
                .collect(Collectors.toList());
    }

    public Optional<AccountResp> findById(Long id) {
        return accountRepo.findById(id).map(facade::toAccountResp);
    }

    public AccountResp save(Long cId, AccountReq aReq) {
        Account a = customerRepo.findById(cId).map(c -> {
            Account account = facade.toAccount(aReq);
            account.setCustomer(c);
            return accountRepo.save(account);
        }).orElseThrow(() -> new RuntimeException("Account wasn't added"));
        return facade.toAccountResp(accountRepo.save(a));
    }

    private Account getByNumber(String number) throws RuntimeException {
        try {
            return accountRepo.findAll().stream().filter(a -> Objects.equals(a.getNumber(), number))
                    .findFirst().orElseThrow();
        } catch (Exception e) {
            throw new RuntimeException("Account doesn't exist!");
        }
    }

    public AccountResp findByNumber(String number) {
        return facade.toAccountResp(getByNumber(number));
    }

    public boolean deleteById(Long id) {
        try {
            accountRepo.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void deleteAll(List<Account> accounts) {
        accountRepo.deleteAll();
    }

    public AccountResp addFunds(String n, Double amount) {
        Account a = getByNumber(n);
        Double b = a.getBalance();
        a.setBalance(b + amount);

        Account saved = accountRepo.save(a);
        return facade.toAccountResp(saved);
    }

    public AccountResp withdraw(String n, Double amount) throws RuntimeException {
        Account a = getByNumber(n);
        Double b = a.getBalance();
        if(b < amount) throw new RuntimeException("Not enough money on your account!");
        a.setBalance(b - amount);

        Account saved = accountRepo.save(a);
        return facade.toAccountResp(saved);
    }

    public boolean transfer(String from, String to, Double amount) {
        try {
            withdraw(from, amount);
            addFunds(to, amount);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
