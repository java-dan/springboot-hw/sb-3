package app.service;

import app.dto.customer.CustomerReq;
import app.dto.customer.CustomerResp;
import app.mapper.CustomerFacade;
import app.models.Customer;
import app.repo.CustomerRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepo customerRepo;
    private final CustomerFacade facade;

    public List<CustomerResp> getAll(Pageable page) {
        return customerRepo.findAll(page)
                .stream()
                .map(facade::toCustomerResp)
                .collect(Collectors.toList());
    }

    private Optional<Customer> getById(Long id) {
        return customerRepo.findById(id);
    }
    public Optional<CustomerResp> findById(Long id) {
        return customerRepo.findById(id).map(facade::toCustomerResp);
    }

    public CustomerResp save(CustomerReq c) {
        Customer customer = facade.toCustomer(c);
        return facade.toCustomerResp(customerRepo.save(customer));
    }

    public Optional<CustomerResp> updateCustomer(Long id, CustomerReq customer){
        Optional<Customer> cc = getById(id);
        if(cc.isPresent()){
            Customer c = facade.toCustomer(customer);
            c.setId(id);
            c.setCreatedDate(cc.get().getCreatedDate());
            c.setPhoneNumber(cc.get().getPhoneNumber());
            c.setAccounts(cc.get().getAccounts());
            c.setEmployers(cc.get().getEmployers());
            return Optional.of(facade.toCustomerResp(customerRepo.saveAndFlush(c)));
        } else{
            return  Optional.empty();
        }
    }

    public boolean deleteById(Long id) {
        try {
            customerRepo.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void deleteAll() {
        customerRepo.deleteAll();
    }
}
