package app.dao;

import app.models.Employer;

public interface EmployerDao extends DAO<Employer> {
}
